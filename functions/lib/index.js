//import { request } from 'request';
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
// For sending email
const nodemailer = require("nodemailer");
// For SMS function
const AWS = require("aws-sdk");
AWS.config.region = "ap-southeast-1";
AWS.config.update({
    accessKeyId: "AKIAJVRURBPPSVEG5FBQ",
    secretAccessKey: "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",
});
const sns = new AWS.SNS();
// Setting up Email account
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: gmailEmail,
        pass: gmailPassword
    }
});
const APP_NAME = "Crowd Distress Application";
exports.sendInviteEmail = functions.database.ref('/invitation/{pushId}/{email}/{username}')
    .onCreate((change, context) => {
    const pushId = context.params.pushId;
    const email = context.params.email;
    const username = context.params.username;
    const downloadUrl = "https://crowddistress.firebaseapp.com/invitation";
    const em = email.split("_");
    const fullEmail = em[0] + "@" + em[1] + "." + em[2];
    const mailOptions = {
        from: APP_NAME + " Admin <noreply@firebase.com>",
        to: fullEmail,
        subject: "Welcome to " + APP_NAME,
        text: "Hi there,\n\n" + username + " has added you as an emergency contact on " + APP_NAME + "."
            + " \nPlease go to " + downloadUrl + " to download the Android APK.\n\n" +
            "Admin, \n" + APP_NAME
    };
    return mailTransport.sendMail(mailOptions).then(() => {
        const ref = admin.database().ref("invitation/" + pushId);
        ref.remove();
    });
});
exports.sendDistressSMS = functions.database.ref('/sms/{pushId}/')
    .onCreate((change, context) => {
    const pushId = context.params.pushId;
    admin.database().ref("sms/" + pushId).remove();
    return admin.database().ref('/adminNo').once("value").then((snapshot) => {
        snapshot.val().forEach(element => {
            console.log(element);
            const number = "+6" + element;
            const params = {
                Message: "A new distress signal is being sent! Visit https://crowddistress.firebaseapp.com for more info.",
                MessageStructure: 'string',
                PhoneNumber: number,
                Subject: "Crowd Distress Application"
            };
            sns.publish(params, function (err, data) {
                if (err)
                    console.log(err, err.stack);
                else
                    console.log(data);
                const ref = admin.database().ref("sms/" + pushId);
                ref.remove();
            });
        });
    });
});
exports.sendRequestPushNotification = functions.database.ref('/requestPush/{targetToken}')
    .onCreate((change, context) => {
    const targetToken = context.params.targetToken;
    const message = {
        token: targetToken,
        notification: {
            title: "You have a new emergency contact request.",
            body: "Touch to react."
        }
    };
    return admin.messaging().send(message)
        .then((response) => {
        console.log('Successfully sent message: ', response);
        const ref = admin.database().ref('requestPush/' + targetToken);
        ref.remove();
    })
        .catch((error) => {
        console.log("Error sending message: ", error);
    });
});
exports.sendDistressPushNotification = functions.database.ref('/contacts/{ownEmail}/ringing/{eventId}')
    .onCreate((change, context) => {
    const ownEmail = context.params.ownEmail;
    const eventId = context.params.eventId;
    return admin.database().ref('/emails/' + ownEmail).once("value").then((snapshot) => {
        admin.database().ref('/fcmTokens/' + snapshot.val()).once("value").then((token) => {
            const message = {
                token: token.val(),
                data: {
                    distress: "true"
                }
            };
            admin.messaging().send(message)
                .then((response) => {
                console.log('Successfully sent message: ', response);
            })
                .catch((error) => {
                console.log("Error sending message: ", error);
            });
        });
    });
});
//# sourceMappingURL=index.js.map