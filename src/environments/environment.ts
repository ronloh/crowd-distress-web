// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBFjNn5TZfOx6ZlLAMQ-JTzUgp_y3yQgbg",
    authDomain: "crowddistress.firebaseapp.com",
    databaseURL: "https://crowddistress.firebaseio.com",
    projectId: "crowddistress",
    storageBucket: "crowddistress.appspot.com",
    messagingSenderId: "688577523448"
  },
  googleMapsKey: 'AIzaSyBQnURndR2mZfJ6RBK75_iN5mQjsHH6D9k'
};
