import { Component, OnInit, ViewChild, ElementRef, enableProdMode } from '@angular/core';
import { AuthService } from './shared/auth.service';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(
    private auth: AuthService
  ){
  }

  ngOnInit(){
  }

  sendTokenToServer() {
  }
}
