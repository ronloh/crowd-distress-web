import { Injectable }  from '@angular/core';
import { CanActivate, Router, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './shared/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    let _url: string = "";

    state.url.split("/").forEach(element => {
        if(_url==="")
            if(element!=="")
                _url=element;
    });

    if(_url === "invitation")
      return true;

    return this.checkLogin();
  }

  checkLogin(): boolean {
    if (localStorage.getItem('uid')) { return true; }

    // Navigate to the login page
    this.router.navigate(['/']);
    return false;
  }
}