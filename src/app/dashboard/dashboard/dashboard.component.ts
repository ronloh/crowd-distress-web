import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  user = null;
  subscription;
  minutesLeft: number;
  secondsLeft: number;

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,

  ) { }

  ngOnInit() {
    /* To log user out after inactive for 20 minutes
    localStorage.setItem('timestamp', new Date().getTime());

    this.subscription = Observable.interval(1000).subscribe(() => {

      let currentTime = new Date().getTime();
      let timestamp = parseInt(localStorage.getItem('timestamp'));

      if (currentTime - timestamp > 1000 * 60 * 20) {
        this.router.navigate(['']);
        this.subscription.unsubscribe();
      } else {
        let timeLeft = (1000 * 60 * 20) - (currentTime - timestamp);

        this.minutesLeft = Math.floor(timeLeft / 60000);
        this.secondsLeft = Math.floor((timeLeft % 60000) / 1000);
      }

    });
*/
    this.user = localStorage.getItem('uid');
  }

  logout() {
    this.auth.logout();
  }

  openManagePhoneNoDialog() {

    let dialogConfig = new MatDialogConfig();

    dialogConfig.height = '400px';
    dialogConfig.width = '600px';
    dialogConfig.disableClose = true;

    let dialogRef = this.dialog.open(ManagePhoneNoDialog, dialogConfig);
  }
}

@Component({
  templateUrl: 'manage-phone-no-dialog.html',
})
export class ManagePhoneNoDialog {

  errorMsg;
  phoneNoList: Array<number> = [];
  @ViewChild('phoneNo') phoneNo: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<ManagePhoneNoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private db: AngularFireDatabase
  ) {
    let dbRef = this.db.database.ref("/adminNo");
    dbRef.once("value").then((dataSnapshot) => {
      dataSnapshot.forEach(snapshot => {
        if (snapshot.val() != null)
          this.phoneNoList.push(snapshot.val());
      })
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onInit() {

  }

  addNumber() {
    let regex = /[0-9]|\./;
    let currentPhoneNo = this.phoneNo.nativeElement.value;

    if (!regex.test(currentPhoneNo)) {
      this.errorMsg = "Invalid phone number";
    } else if (!(currentPhoneNo.length == 10 || currentPhoneNo.length == 11)) {
      this.errorMsg = "Phone number must be 10 or 11 characters";
    } else {
      this.errorMsg = ""
      let currPhoneNo = this.phoneNo.nativeElement.value;
      let boolAdded = false;

      let dbRef = this.db.database.ref("/adminNo");
      dbRef.once("value").then((dataSnapshot) => {

        dataSnapshot.forEach(snapshot => {
          if (snapshot.val() == currPhoneNo) {
            this.errorMsg = "This number has already been added."
            boolAdded = true;
          }
        })

        if (!boolAdded) {
          this.phoneNoList.push(this.phoneNo.nativeElement.value);
          this.db.database.ref("/adminNo").set(this.phoneNoList);
          this.phoneNo.nativeElement.value = "";
        }
      });
    }
  }

  deleteNumber(index) {
    if (confirm("Are you sure you want to delete this number?")) {
      this.db.database.ref("/adminNo").child(index).remove();
      this.phoneNoList.splice(index, 1);
      let dbRef = this.db.database.ref("/adminNo");
      dbRef.set(this.phoneNoList);
    }
  }
}
