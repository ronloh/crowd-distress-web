import { Component, OnInit, Inject } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AngularFireDatabase } from 'angularfire2/database';
import { BehaviorSubject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { Lightbox, LightboxConfig } from 'angular2-lightbox';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})
export class GoogleMapComponent implements OnInit {

  dbRef: any;
  lat: any;
  lng: any;
  markers: Array<any> = [];
  _album: Array<any> = [];
  user: any;
  distressClicked = true;
  pastDistressClicked = false;
  eventClicked = false;
  totalDistress;
  totalPastDistress;
  totalEvents;

  constructor(
    private db: AngularFireDatabase,
    private dialog: MatDialog,
    private _lightbox: Lightbox,
    private _lightboxConfig: LightboxConfig
  ) {
    _lightboxConfig.fitImageInViewPort = true;
    _lightboxConfig.centerVertically = true;
  }

  ngOnInit() {

    this.getUserLocation();

    // Show currently ringing distress signal onInit
    this.dbRef = this.db.database.ref('/events/distress');
    this.dbRef.on("value", (snapshot) => {

      this.totalDistress = 0;

      snapshot.forEach(element => {

        this.dbRef = this.db.database.ref('users/' + element.val().uid);
        this.dbRef.on("value", (snapshot) => {

          this.user = {
            name: snapshot.val().name,
            phoneNo: snapshot.val().phoneNo,
          }

          let marker = {
            lat: parseFloat(element.val().lat),
            lng: parseFloat(element.val().lng),
            type: element.val().type,
            timestamp: parseInt(element.val().timestamp),
            userName: this.user.name,
            phoneNo: this.user.phoneNo,
            uid: element.val().uid,
            eventLevel: "distress",
            eventid: element.key
          }

          let currentMarkers = this.markers;
          currentMarkers.push(marker);
        });

        this.totalDistress++;
      });
    });

    this.totalPastDistress = 0;
    this.dbRef = this.db.database.ref('/events/pastDistress');
    this.dbRef.on("value", (snapshot) => {
      snapshot.forEach(element => {
        this.totalPastDistress++;
      })
    })

    this.totalEvents = 0;
    this.dbRef = this.db.database.ref('/events/normal');
    this.dbRef.on("value", (snapshot) => {
      snapshot.forEach(element => {
        this.totalEvents++;
      })
    })
  }

  openUserProfileDialog(uid: String) {

    let dialogConfig = new MatDialogConfig();

    this.dbRef = this.db.database.ref('users/' + uid);
    this.dbRef.on("value", (snapshot) => {
      dialogConfig.data = {
        "name": snapshot.val().name,
        "phoneNo": snapshot.val().phoneNo,
        "gender": snapshot.val().gender,
        "dob": snapshot.val().date,
        "height": snapshot.val().height,
        "weight": snapshot.val().weight,
        "race": snapshot.val().race,
        "imageUrl": snapshot.val().imageUrl
      };
    });

    dialogConfig.height = '400px';
    dialogConfig.width = '600px';
    dialogConfig.disableClose = true;

    let dialogRef = this.dialog.open(UserProfileDialog, dialogConfig);
  }

  openEventPhotoDialog(eventLevel: String, eventid: String) {
    this._album = [];
    this.dbRef = this.db.database.ref('events/' + eventLevel + "/" + eventid + "/imageUrl");
    this.dbRef.on("value", (snapshot) => {

      snapshot.forEach(element => {
        const album = {
          src: element.val()
        }

        this._album.push(album);
      })

      this._lightbox.open(this._album, 0);
    });
  }

  private getUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      })
    }
  }

  private switchMarker(type) {
    switch (type) {

      case "distress":
        this.eventClicked = false;
        this.distressClicked = true;
        this.pastDistressClicked = false;

        this.markers = [];

        this.dbRef = this.db.database.ref('/events/distress');
        this.dbRef.on("value", (snapshot) => {

          this.totalDistress = 0;

          snapshot.forEach(element => {
            this.dbRef = this.db.database.ref('users/' + element.val().uid);
            this.dbRef.on("value", (snapshot) => {

              this.user = {
                name: snapshot.val().name,
                phoneNo: snapshot.val().phoneNo,
              }

              let marker = {
                lat: parseFloat(element.val().lat),
                lng: parseFloat(element.val().lng),
                type: element.val().type,
                timestamp: parseInt(element.val().timestamp),
                userName: this.user.name,
                phoneNo: this.user.phoneNo,
                uid: element.val().uid,
                eventLevel: "distress",
                eventid: element.key
              }

              let currentMarkers = this.markers;
              currentMarkers.push(marker);
            });

            this.totalDistress++;

          });
        })
        break;

      case "pastDistress":
        this.eventClicked = false;
        this.distressClicked = false;
        this.pastDistressClicked = true;

        this.markers = [];

        this.dbRef = this.db.database.ref('/events/pastDistress');
        this.dbRef.on("value", (snapshot) => {

          this.totalPastDistress = 0;

          snapshot.forEach(element => {

            this.dbRef = this.db.database.ref('users/' + element.val().uid);
            this.dbRef.on("value", (snapshot) => {

              this.user = {
                name: snapshot.val().name,
                phoneNo: snapshot.val().phoneNo,
              }

              let marker = {
                lat: parseFloat(element.val().lat),
                lng: parseFloat(element.val().lng),
                type: element.val().type,
                timestamp: parseInt(element.val().timestamp),
                userName: this.user.name,
                phoneNo: this.user.phoneNo,
                uid: element.val().uid,
                eventLevel: "pastDistress",
                eventid: element.key
              }

              let currentMarkers = this.markers;
              currentMarkers.push(marker);
            });

            this.totalPastDistress++;

          });
        })
        break;

      case "events":
        this.eventClicked = true;
        this.distressClicked = false;
        this.pastDistressClicked = false;

        this.markers = [];

        this.dbRef = this.db.database.ref('/events/normal');
        this.dbRef.on("value", (snapshot) => {

          this.totalEvents = 0;

          snapshot.forEach(element => {

            this.dbRef = this.db.database.ref('users/' + element.val().uid);
            this.dbRef.on("value", (snapshot) => {

              this.user = {
                name: snapshot.val().name,
                phoneNo: snapshot.val().phoneNo,
              }

              let marker = {
                lat: parseFloat(element.val().lat),
                lng: parseFloat(element.val().lng),
                type: element.val().type,
                timestamp: parseInt(element.val().timestamp),
                userName: this.user.name,
                phoneNo: this.user.phoneNo,
                uid: element.val().uid,
                eventLevel: "normal",
                eventid: element.key
              }

              let currentMarkers = this.markers;
              currentMarkers.push(marker);
            });

            this.totalEvents++;

          });
        })
        break;
    }
  }
}

@Component({
  templateUrl: 'user-profile-dialog.html',
})
export class UserProfileDialog {

  constructor(
    public dialogRef: MatDialogRef<UserProfileDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}