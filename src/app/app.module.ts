import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule, ApplicationRef } from '@angular/core';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth'
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LightboxModule } from 'angular2-lightbox';

import { environment } from '../environments/environment';
import { AuthService } from './shared/auth.service';
import { AuthGuard } from './auth-guard.service';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { UserProfileDialog } from './google-map/google-map.component';
import { ManagePhoneNoDialog } from './dashboard/dashboard/dashboard.component';
import { InvitationComponent } from './invitation/invitation/invitation.component';
import { AgmCoreModule } from '@agm/core';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'invitation', component: InvitationComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    GoogleMapComponent,
    InvitationComponent,
    UserProfileDialog,
    ManagePhoneNoDialog
  ],

  imports: [
    BrowserModule,
    CommonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    LightboxModule,
    AngularFireModule.initializeApp(environment.firebase, 'crowd-distress'),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    RouterModule.forRoot(appRoutes),
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsKey
    })
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [UserProfileDialog, ManagePhoneNoDialog]
})
export class AppModule {}
