import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

@Injectable()
export class AuthService {
  static AUTH_STATE: any = null;

  constructor(private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private router: Router) {

    this.afAuth.auth.setPersistence(firebase.auth.Auth.Persistence.SESSION);

    this.afAuth.authState.subscribe((auth) => {
      AuthService.AUTH_STATE = auth
    });
  }

  // Returns true if user is logged in
  get authenticated(): boolean {
    return AuthService.AUTH_STATE !== null;
  }

  // Returns current user data
  get currentUser(): any {
    return this.authenticated ? AuthService.AUTH_STATE : null;
  }

  // Returns
  get currentUserObservable(): any {
    return this.afAuth.authState
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated ? AuthService.AUTH_STATE.uid : '';
  }

  loginWithEmail(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        // Only the "admin" user can login
        if (user.uid == '0WqKf8T8wVXmTxszWJSkTErTjyr1') {
          AuthService.AUTH_STATE = user;

          let currentTime = new Date().getTime();
          localStorage.setItem('uid', user.uid);
          localStorage.setItem('timestamp', currentTime.toString());
        } else {
          this.logout();
          //throw "invalid-login";
        }
      });
  }

  logout(): void {
    this.afAuth.auth.signOut();
    localStorage.clear();
    this.router.navigate(['/']);
  }

}
