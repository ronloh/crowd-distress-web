import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../../../environments/environment.prod';


@Component({
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css'],
})
export class InvitationComponent implements OnInit {
  invitationTitle = 'Crowd Distress Invitation Site';
  downloadUrl;
  version = "retrieving...";
  versionStage = "";
  versionDate = "";

  constructor(
    private db: AngularFireDatabase
  ) {
      firebase.initializeApp(environment.firebase);
      this.downloadUrl = "";

      let storage = firebase.storage();
      let pathRef = storage.ref('apk/Crowd_Distress.apk');

      pathRef.getDownloadURL().then((url) => {
        this.downloadUrl = url;
      });

      firebase.database().ref("apkDate/version").once("value").then((snapshot) => {

        this.versionStage = snapshot.val();

        firebase.database().ref("apkDate/updatedDate").once("value").then((snapshot) => {
          this.versionDate = snapshot.val();

          this.version = this.versionStage + " (last updated: " + this.versionDate + ")";
        });

      });


  }

  ngOnInit() {
  }

}
