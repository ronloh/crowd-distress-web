import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { InvitationComponent } from './invitation/invitation.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule
  ],
  declarations: []
})
export class InvitationModule { }
