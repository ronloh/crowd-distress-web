# CrowdDistress

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Firebase Setup

This project is based on the Firebase Hosting, Database, and Cloud Function.

## Create Project

If you haven't already create a Firebase project, go to https://console.firebase.google.com/ to create a new Firebase project.

Please use the same project for both Android and Web parts as they are sharing databases.

After successfully created your new project, choose "Add Firebase to your web app" and copy the data in the "config" JSON to the following files:

1) src/environments/environments.ts
2) src/environments/environments.prod.ts

## Deploy Project

Refers https://firebase.google.com/docs/hosting/deploying for documentation.

You may skip the "firebase init" command as the latest source already done that.

Please run a "ng build" first before each Firebase Hosting deploy.

For first time deployment, you may use "firebase deploy" to deploy all settings.

During development, you may want to do "firebase deploy --only hosting" or "firebase deploy --only functions" depends on the part of update you want to commit.

